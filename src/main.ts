import { createApp } from "vue";
import App from "./App.vue";
import Equal from "equal-vue";
import "equal-vue/dist/style.css";
import "./assets/styles/style.css";
import "./assets/styles/panel.css";
import "./assets/styles/loading.css";
createApp(App).use(Equal).mount("#app");
