const removeCharAt = (index: number, str: string): string => {
  return str.slice(0, index) + str.slice(index + 1);
};

const ajouterMot = (
  mot: string,
  map: Map<string, string[]>
): Map<string, string[]> => {
  const premiereLettre: string = mot[0];
  let liste_mots: string[] | undefined;

  if ((liste_mots = map.get(premiereLettre))) {
    liste_mots.push(mot);
  } else {
    liste_mots = [mot];
  }

  map.set(premiereLettre, liste_mots);
  return map;
};

export const creerJSON = (
  listeMots: string[]
): Map<number, Map<string, string[]>> => {
  let map_six: Map<string, string[]> = new Map<string, string[]>();
  let map_sept: Map<string, string[]> = new Map<string, string[]>();
  let map_huit: Map<string, string[]> = new Map<string, string[]>();
  let map_neuf: Map<string, string[]> = new Map<string, string[]>();

  for (const mot of listeMots) {
    switch (mot.length) {
      case 6:
        map_six = ajouterMot(mot, map_six);
        break;

      case 7:
        map_sept = ajouterMot(mot, map_sept);
        break;

      case 8:
        map_huit = ajouterMot(mot, map_huit);
        break;

      case 9:
        map_neuf = ajouterMot(mot, map_neuf);
        break;

      default:
        break;
    }
  }

  const dico: Map<number, Map<string, string[]>> = new Map<
    number,
    Map<string, string[]>
  >();
  dico.set(6, map_six);
  dico.set(7, map_sept);
  dico.set(8, map_huit);
  dico.set(9, map_neuf);
  return dico;
};

export const checkModele = (mot: string, modele: string) => {
  let check: boolean = mot.length === modele.length,
    index: number = 0;

  while (check && index < mot.length) {
    if (modele[index] !== "?" && modele[index] !== mot[index]) {
      check = false;
    } else {
      index++;
    }
  }

  return check;
};

export const checkMot = (
  mot: string,
  modele: string,
  lettresMalPlacees: string,
  lettresExclues: string
) => {
  let contientLettresExclues: boolean = false,
    i: number,
    index: number,
    lettre: string;

  for (i = 0; i < modele.length; i++) {
    if (modele[i] === "?") {
      lettre = mot[i];
      index = lettresMalPlacees.indexOf(lettre);

      //Check si c'est une lettre mal placée
      if (index >= 0) {
        lettresMalPlacees = removeCharAt(index, lettresMalPlacees);
      }

      //Sinon check c'est une lettre exclue
      else if (lettresExclues.includes(lettre)) {
        contientLettresExclues = true;
        break;
      }
    }
  }

  return !contientLettresExclues && lettresMalPlacees.length === 0;
};
