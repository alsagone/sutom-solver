## SUTOM Solver

### par alsagone

#### Le dictionnaire

Ce qui m'a poussé à coder ce site c'est la découverte que le dictionnaire des mots acceptés par SUTOM était accessible via le code JavaScript de la page.

Donc j'ai tout copié-collé dans un fichier texte puis m'est venue l'idée de regrouper les mots par longueur puis par première lettre - pour éviter de parcourir les 149 440 mots du dictionnaire à chaque recherche.

D'un point de vue structure de données, j'ai utilisé une Map qui associe la longueur du mot à une autre Map qui elle associe la première lettre du mot et la liste de mots commençant par cette lettre.
Par exemple, si je veux accéder aux mots de 7 lettres commençant par `'a'`, je fais: `liste = dico.get(7).get('a')`

#### Comment savoir si un mot est jouable ou non ?

En guise d'exemple, prenons cette grille

![grille](../grille.webp)

On a donc:

- Modèle: `A??O???`
- Lettres mal placées (🟡): `NI`
- Lettres exclues (🟦): `MADUER`

Pour qu'un mot soit jouable, il faut à la fois qu'il:

- respecte le modèle
- contienne toutes les lettres mal placées
- ne contienne aucune des lettres exclues

### Vérification du modèle

Par exemple, vérifions si _ABDOMEN_ vérifie notre modèle.
On parcourt chaque lettre du modèle.
Si on tombe sur autre chose qu'un `?`, on vérifie si la lettre est à la même position dans le mot.
Ici avec notre modèle, le "O" est en 4ème position et il est aussi en 4ème position dans "ABDOMEN" donc tout va bien.

### Vérification des lettres mal placées / exclues

On parcourt chaque lettre du modèle.
Si on tombe sur un `?`, on prend la lettre qui est à la même position dans le mot.
Avec notre modèle, le premier `?` est en 2è position donc on regarde si le "B" d'ABDOMEN vérifie nos conditions.

On vérifie d'abord si c'est une des lettres mal placées.
Si oui, on supprime la lettre des lettres mal placées.

Si non, on vérifie si c'est une des lettres exclues.
Si c'est le cas, on sait d'office que ce n'est pas un mot jouable.
Ici en l'occurence, le "M" est une des lettres exclues donc ABDOMEN n'est pas un mot jouable.

Si on finit de parcourir le modèle sans rencontrer de lettres exclues, on sait si le mot est jouable si et seulement si toutes les lettres mal placées ont été utilisées.

![code](code.webp)

#### Au final

La fonction chargée de trouver les mots trouve d'abord la liste de mots à tester en se basant sur la longueur du mot et sa première lettre.
Puis filtre la liste en ne gardant que les mots qui respectent le modèle et les lettres mal placées/exclues.

Et voilà :)
